from urllib import urlretrieve
import os
import csv
import sys
import xlsxwriter
#from xlsxwriter.workbook import Workbook


def saveFile():
	fName = "./Data.csv"
	urlStr="https://data.cityofchicago.org/api/views/ijzp-q8t2/rows.csv?accessType=DOWNLOAD"
	try:
	    urlretrieve(urlStr,fName)
	except Exception as e:
	    print e
	    sys.exit(0)

def readAndWrite():
	try:
	    fName = "./Data.csv"
	    csvFile = open(fName,'r')
	    spamreader = csv.reader(csvFile,delimiter=' ')
	    workbook = xlsxwriter.Workbook('Data.xlsx')
	    worksheet = workbook.add_worksheet()
	    row = 0
	    col = 0
	    for i in spamreader:
	        for j in i:
	            worksheet.write(row, col, j)
	            col+=1
	        row+=1
	        col=0
	    workbook.close()
	except Exception as e:
	    print e
	    sys.exit(0)

if __name__ == "__main__":
    saveFile()
    readAndWrite()
else:
    saveFile()
    readAndWrite()
